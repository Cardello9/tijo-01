class Main {
    // rzuca wyjatek EmptyDataException gdy parametr wejsciowy jest pusty
    private double getAverage(final String digits) throws EmptyDataException {
        // prosze zaimplementowac algorytm do wyliczania sredniej
        // arytmetycznej z podanego ciagu liczb
        // naglowek metody nie moze ulec zmianie!
        if(digits.equals("")) {
            throw new EmptyDataException();
        }
        String strArr[] = digits.split(",");
        double sum = 0;
        for(int i=0; i<strArr.length; i++) {
           sum += Double.parseDouble(strArr[i]);
        }
        return sum / strArr.length;
        }
        public static void main(String[] args) {
        final String listOfDigits ="1.0,2.0,3.5,4.5,5.0,5.5,6.0,10.1,5.2,4.3,2.1,1.1";
        final Main main = new Main();
        try {
            double average = main.getAverage(listOfDigits);
            System.out.printf("avg(%s) = %.2f \n", listOfDigits, average);
        } catch(EmptyDataException e) {
            System.out.println("Lista nie moze byc pusta!");}
    }
}