interface RulesOfGame {
    // metoda sprawdza czy ruch jest poprawny dla figury.
    boolean isCorrectMove(Point2d moveFrom, Point2d moveTo);
}
