class Point2d {
    private int x;
    private int y;

    Point2d(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    public String toString() {
        return "(" + String.valueOf(this.getX()) + ", " + String.valueOf(this.getY()) + ")";
    }
    // prosze dokonczyc implementacje klasy Point2d o konstruktor,
    // publiczne metody zwracajace poszczegolne wspolrzedne oraz metode toString,
    // ktora zwraca wspolrzedne w odpowiednim formacie.}
}