public class Main {
    public static void main(String[] args) {
        System.out.println("Test for ROOK");
        final RulesOfGame rook = new Rook();
        Point2d pointFrom = new Point2d(0, 0);
        Point2d pointTo = new Point2d(0, 10);
        System.out.printf("From %10s to %10s is %s \n",pointFrom, pointTo,rook.isCorrectMove(pointFrom, pointTo)); // true
        pointFrom = new Point2d(-10, -1);pointTo = new Point2d(10, -1);
        System.out.printf("From %10s to %10s is %s \n",pointFrom, pointTo,rook.isCorrectMove(pointFrom, pointTo)); // true
        pointFrom = new Point2d(-1, -1);
        pointTo = new Point2d(5, 5);
        System.out.printf("From %10s to %10s is %s \n",pointFrom, pointTo,rook.isCorrectMove(pointFrom, pointTo)); // false
        pointFrom = new Point2d(1, 1);pointTo = new Point2d(-1, -2);
        System.out.printf("From %10s to %10s is %s \n",pointFrom, pointTo,rook.isCorrectMove(pointFrom, pointTo)); // false
        }
}